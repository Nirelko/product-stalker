package com.productstalker.fragments.addProductsFragment;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.productstalker.R;
import com.productstalker.adapters.CommitProductListAdapter;
import com.productstalker.fragments.deleteFragment.DeleteFragment;
import com.productstalker.models.Product;
import com.productstalker.obtainers.ObtainersManager;
import com.productstalker.obtainers.exceptions.CannotObtainProductException;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.functions.Action1;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddProductFragment extends DeleteFragment {
    IProductUpdaterActivity productUpdaterActivity;
    ArrayList<Product> products;
    CommitProductListAdapter commitProductsAdapter;
    Menu menu;

    public AddProductFragment() {
        // Required empty public constructor
    }

     @Override
     public void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);

         setHasOptionsMenu(true);
     }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        productUpdaterActivity = (IProductUpdaterActivity) context;
        products = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setTitle();

        return inflater.inflate(R.layout.fragment_add_product, container, false);
    }

    private void setTitle() {
        ((AppCompatActivity)context).getSupportActionBar().setTitle(R.string.add_product_fragment_title);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_add_product, menu);

        this.menu = menu;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.confirm_selection){
            productUpdaterActivity.addProducts(products);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        bindCommitProductsList();

        observeUrlForPreview();

        overrideFragmentButtonListeners();

        handleSharedUrl();
    }

    private void bindCommitProductsList() {
        final Activity activity = getActivity();
        ListView commitProductsView = (ListView) activity.findViewById(R.id.commitProducts);

        commitProductsAdapter = new CommitProductListAdapter(activity,
                R.layout.commit_product_list_item, products, selectedItems);

        commitProductsView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        commitProductsView.setMultiChoiceModeListener(this);
        commitProductsView.setAdapter(commitProductsAdapter);
    }

    private void observeUrlForPreview() {
        final Activity activity = getActivity();

        Observable<String> urlObserver = RxTextView.textChanges((EditText)activity.findViewById(R.id.productUrl))
                .debounce(600, TimeUnit.MILLISECONDS)
                .map(url ->  url.toString());

        urlObserver.subscribe(url -> {
            if(url.length() < 4){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        activity.findViewById(R.id.productPreviewContainer).setVisibility(View.INVISIBLE);
                    }
                });
            }
            else{
                final String productDetails = productUpdaterActivity.getProductDetails(url);

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setPreviewViews(productDetails);

                        activity.findViewById(R.id.productPreviewContainer).setVisibility(View.VISIBLE);
                    }
                });

            }
        });
    }

    private void overrideFragmentButtonListeners() {
        Activity activity = getActivity();
        ImageButton addToCommitList = (ImageButton)activity.findViewById(R.id.addProductToCommit);

        addToCommitList.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onAddProductToCommitClicked();
            }
        });
    }

    private void handleSharedUrl() {
        Bundle args = getArguments();

        if(args == null || args.isEmpty()){
            return;
        }

        Activity activity = getActivity();

        tryAddToCommit(activity, args.getString(getString(R.string.shared_url_arg_name)));
    }

    private void setPreviewViews(String productDetails) {
        ((TextView)getActivity().findViewById(R.id.productPreviewName)).setText(productDetails);
    }


    private void onAddProductToCommitClicked() {
        Activity activity = getActivity();
        EditText urlEditText = (EditText)activity.findViewById(R.id.productUrl);

        tryAddToCommit(activity, urlEditText.getText().toString());

        urlEditText.setText("");

        hideKeyboard(activity);
    }

    private void tryAddToCommit(Activity activity, String productUrl) {
        final ProgressDialog progressDialog = ProgressDialog.show(context, "Loading Product", "Will just take a moment", true);

        try {
            ObtainersManager.getInstance().getFullProduct(productUrl, new Action1<Product>(){
                @Override
                public void call(Product product) {
                    commitProductsAdapter.add(product);
                    commitProductsAdapter.notifyDataSetChanged();

                    setMultiAddConfirmIcon();
                    progressDialog.dismiss();

                }
            });
        } catch (CannotObtainProductException e) {
            progressDialog.dismiss();
            // If we will ever log the stackrace XD
            // TODO: move to logger class that will handle the type of the exception, now im testing!!!
            Toast.makeText(activity, "Could not load the product from the url", Toast.LENGTH_LONG).show();
        }
        commitProductsAdapter.notifyDataSetChanged();
    }

    private void hideKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void setMultiAddConfirmIcon() {
        if(products.size() > 1) {
            this.menu.findItem(R.id.confirm_selection).setIcon(R.drawable.ic_commit_multiple_selection);
        }
        else{
            this.menu.findItem(R.id.confirm_selection).setIcon(R.drawable.ic_commit_selection);
        }
    }

    @Override
    public void onItemCheckedStateChanged(ActionMode actionMode, int position, long id, boolean checked) {
        super.onItemCheckedStateChanged(actionMode, position, id, checked);

        commitProductsAdapter.notifyDataSetChanged();
    }

    @Override
    protected void PerformDelete() {
        for(int index = 0; index < selectedItems.size(); index++){
            products.remove((int)selectedItems.get(index));
        }

        commitProductsAdapter.notifyDataSetChanged();
    }
}
