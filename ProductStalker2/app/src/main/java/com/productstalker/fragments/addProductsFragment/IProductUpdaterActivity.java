package com.productstalker.fragments.addProductsFragment;

import com.productstalker.models.Product;

import java.util.ArrayList;

/**
 * Created by dindush on 10/28/2016.
 */

public interface IProductUpdaterActivity {
    void addProducts(ArrayList<Product> newProducts);
    String getProductDetails(String url);
}
