package com.productstalker.fragments.productsListFragment;

import android.widget.ArrayAdapter;

import com.productstalker.models.Product;

import java.util.ArrayList;

/**
 * Created by Nirel on 26/10/2016.
 */

public interface IProductObtainerActivity {
    void toggleFavorite(Product product);
    void removeProducts(ArrayList<Integer> productsToRemove);
    void productClicked(Product tag);
}
