package com.productstalker.fragments.productsListFragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.productstalker.R;
import com.productstalker.adapters.ProductListAdapter;
import com.productstalker.fragments.deleteFragment.DeleteFragment;
import com.productstalker.models.Product;

/**
 * A placeholder fragment containing a simple view.
 */
public class ProductsListFragment extends DeleteFragment {
    private Menu menu;
    private IProductObtainerActivity productObtainerActivity;
    private ProductListAdapter productsAdapter;

    public ProductsListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setTitle();

        return inflater.inflate(R.layout.fragment_products_list, container, false);
    }

    private void setTitle() {
        ((AppCompatActivity)context).getSupportActionBar().setTitle(R.string.product_list_fragment_title);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        Activity activity = getActivity();
        initProductsList(activity);
        initProductListView(activity);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.productObtainerActivity = (IProductObtainerActivity)context;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        this.menu = menu;
    }

    private void initProductsList(Context activity){
        Bundle args = getArguments();
        productsAdapter = new ProductListAdapter(activity, R.layout.product_list_item, args.getParcelableArrayList("products"), selectedItems);

    }

    private void initProductListView(Activity activity) {
        ListView productsListView = (ListView) activity.findViewById(R.id.productsListView);

        productsListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        productsListView.setMultiChoiceModeListener(this);
        productsListView.setAdapter(productsAdapter);
        productsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                productObtainerActivity.productClicked(productsAdapter.getProduct(position));
            }
        });
    }

    @Override
    public void onItemCheckedStateChanged(ActionMode actionMode, int position, long id, boolean checked) {
        super.onItemCheckedStateChanged(actionMode, position, id, checked);

        productsAdapter.notifyDataSetChanged();
    }

    @Override
    protected void PerformDelete() {
        productObtainerActivity.removeProducts(selectedItems);

        productsAdapter.notifyDataSetChanged();
    }
}
