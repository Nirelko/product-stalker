package com.productstalker.fragments.fullProductFragment;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.productstalker.R;
import com.productstalker.fragments.productsListFragment.IProductObtainerActivity;
import com.productstalker.models.Product;

import static android.support.v4.content.ContextCompat.*;

/**
 * A simple {@link Fragment} subclass.
 */
public class FullProductFragment extends Fragment {
    Product _watchedProduct;
    IProductObtainerActivity _productObtainer;

    public FullProductFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_full_product, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        _productObtainer = (IProductObtainerActivity)getActivity();
        initProduct();
        setTitle();
    }

    private void setTitle() {
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.full_product_fragment_title);
    }

    private void initProduct() {
        _watchedProduct = getArguments().getParcelable("product");
        Activity activity = getActivity();

        ((ImageView) activity.findViewById(R.id.fullProductImage)).setImageBitmap(_watchedProduct.thumbnailPic);
        ((TextView) activity.findViewById(R.id.fullProductName)).setText(_watchedProduct.name);
        ((TextView) activity.findViewById(R.id.fullProductPrice)).setText(_watchedProduct.price.toString());

        setStockImage(activity);
        setFavoriteListener(activity);
        setOnGoToSiteListener(activity);
    }

    public void setStockImage(Activity activity) {
        ImageView inStockImageView = (ImageView)activity.findViewById(R.id.fullProductStockStatus);

        if (_watchedProduct.isInStock) {
            inStockImageView.setImageResource(R.drawable.ic_in_stock);
            inStockImageView.setColorFilter(getColor(activity, R.color.colorInStock));
        }
        else {
            inStockImageView.setImageResource(R.drawable.ic_out_of_stock);
            inStockImageView.setColorFilter(getColor(activity, R.color.colorOutStock));
        }
    }

    public void setFavoriteListener(final Activity activity) {
        final ImageButton favoriteImageButton = (ImageButton)activity.findViewById(R.id.fullProductFavoriteButton);

        favoriteImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _productObtainer.toggleFavorite(_watchedProduct);

                setFavoriteButtonColor(favoriteImageButton, activity);
            }
        });

        setFavoriteButtonColor(favoriteImageButton, activity);
    }

    private void setFavoriteButtonColor(ImageButton favoriteImageButton, Activity activity) {
        if(_watchedProduct.isFavorite){
            favoriteImageButton.setColorFilter(ContextCompat.getColor(activity, R.color.colorAccentLight));
        }
        else{
            favoriteImageButton.setColorFilter(0);
        }
    }

    public void setOnGoToSiteListener(final Activity activity) {
        activity.findViewById(R.id.fullProductGoToSiteButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(_watchedProduct.url)));
            }
        });
    }
}
