package com.productstalker.fragments.deleteFragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AbsListView;

import com.productstalker.R;

import java.util.ArrayList;

/**
 * Created by Nirel on 05/11/2016.
 */

public abstract class DeleteFragment extends Fragment implements AbsListView.MultiChoiceModeListener {
    protected ArrayList<Integer> selectedItems;
    protected Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

        selectedItems = new ArrayList<>();
    }

    @Override
    public void onItemCheckedStateChanged(ActionMode actionMode, int position, long id, boolean checked) {
        if(checked){
            selectedItems.add(position);
        }
        else{
            selectedItems.remove(selectedItems.indexOf(position));
        }

        actionMode.setTitle(String.format("%1$d", selectedItems.size()));
    }

    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        actionMode.getMenuInflater().inflate(R.menu.menu_remove_product, menu);

        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(final ActionMode actionMode, MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.action_delete:{
                new AlertDialog.Builder(context)
                        .setMessage("Are you sure you want to delete these products?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                PerformDelete();
                                actionMode.finish();
                            }})
                        .setNegativeButton(android.R.string.no, null).show();

                return true;
            }
        }

        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode) {
        selectedItems.clear();
    }

    protected abstract void PerformDelete();
}
