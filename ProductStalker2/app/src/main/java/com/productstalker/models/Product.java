package com.productstalker.models;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Nirel on 29/10/2016.
 */

public class Product implements Parcelable {
    // Come on sql lite.... why not guid!?
    public Long id;
    public String url;
    public String thumbnailPicUrl;
    public Bitmap thumbnailPic;
    public Bitmap ciricleThumbnailPic;
    public String name;
    public Float price;
    public boolean isInStock;
    public boolean isFavorite;

    public Product(){

    }

    // For paracable
    protected Product(Parcel in) {
        id = in.readLong();
        url = in.readString();
        thumbnailPicUrl = in.readString();
        thumbnailPic = Bitmap.CREATOR.createFromParcel(in);
        ciricleThumbnailPic = Bitmap.CREATOR.createFromParcel(in);
        name = in.readString();
        price = in.readFloat();
        isInStock = in.readByte() != 0;
        isFavorite = in.readByte() != 0;
    }

    // Whatever you want....
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeLong(id);
        parcel.writeString(url);
        parcel.writeString(thumbnailPicUrl);
        thumbnailPic.writeToParcel(parcel, 0);
        ciricleThumbnailPic.writeToParcel(parcel, 0);
        parcel.writeString(name);
        parcel.writeFloat(price);
        parcel.writeByte((byte) (isInStock ? 1 : 0));
        parcel.writeByte((byte) (isFavorite ? 1 : 0));
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
}
