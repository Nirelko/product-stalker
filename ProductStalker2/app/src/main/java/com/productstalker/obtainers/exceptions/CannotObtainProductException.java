package com.productstalker.obtainers.exceptions;

import java.io.IOException;

/**
 * Created by Nirel on 29/10/2016.
 */

public class CannotObtainProductException extends IOException {
    public CannotObtainProductException(){
        super();
    }

    public CannotObtainProductException(String message){
        super(message);
    }

    public CannotObtainProductException(String message, Throwable cause){
        super(message, cause);
    }
}
