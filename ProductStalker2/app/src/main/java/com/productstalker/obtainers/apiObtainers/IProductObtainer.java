package com.productstalker.obtainers.apiObtainers;

import com.productstalker.models.Product;

import java.net.URL;

/**
 * Created by Nirel on 29/10/2016.
 */

public interface IProductObtainer {
    Product getProduct(URL url);
}
