package com.productstalker.obtainers.exceptions;

/**
 * Created by Nirel on 29/10/2016.
 */

public class NotSupportedObtainerException extends CannotObtainProductException {
    public NotSupportedObtainerException() {
        super();
    }

    public NotSupportedObtainerException(String message) {
        super(message);
    }

    public NotSupportedObtainerException(String message, Throwable cause) {
        super(message, cause);
    }
}
