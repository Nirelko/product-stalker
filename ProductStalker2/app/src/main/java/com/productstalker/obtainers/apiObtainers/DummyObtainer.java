package com.productstalker.obtainers.apiObtainers;

import com.productstalker.models.Product;

import java.net.URL;

/**
 * Created by Nirel on 31/10/2016.
 */

public class DummyObtainer implements IProductObtainer {
    private Long counter = 0L;
    @Override
    public Product getProduct(URL url) {
        Product product = new Product();
        product.id = counter++;
        product.thumbnailPicUrl = "https://images-na.ssl-images-amazon.com/images/I/713PL%2BuGegL._SX522_.jpg";
        product.name = "Sound Blaster Z PCIe Gaming Sound Card";
        product.price = 69.99f;
        product.isInStock = true;
        product.isFavorite = false;

        return product;
    }
}
