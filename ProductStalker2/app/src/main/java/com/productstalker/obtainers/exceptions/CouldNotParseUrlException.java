package com.productstalker.obtainers.exceptions;

/**
 * Created by Nirel on 29/10/2016.
 */

public class CouldNotParseUrlException extends CannotObtainProductException {
    public CouldNotParseUrlException() {
        super();
    }

    public CouldNotParseUrlException(String message) {
        super(message);
    }

    public CouldNotParseUrlException(String message, Throwable cause) {
        super(message, cause);
    }
}
