package com.productstalker.obtainers;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ArrayAdapter;

import com.productstalker.adapters.AsyncTasks.LoadProductAsyncTask;
import com.productstalker.obtainers.apiObtainers.DummyObtainer;
import com.productstalker.obtainers.apiObtainers.IProductObtainer;
import com.productstalker.models.Product;
import com.productstalker.obtainers.exceptions.CannotObtainProductException;
import com.productstalker.obtainers.exceptions.CouldNotParseUrlException;
import com.productstalker.obtainers.exceptions.NotSupportedObtainerException;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import rx.functions.Action0;
import rx.functions.Action1;

/**
 * Created by Nirel on 29/10/2016.
 */

public class ObtainersManager {
    private HashMap<String, IProductObtainer> obtainers;

    private static ObtainersManager _intance;

    private ObtainersManager(){
        obtainers = new HashMap<>();

        obtainers.put("amazon", new DummyObtainer());
    }

    public static ObtainersManager getInstance(){
        if(_intance == null){
            _intance = new ObtainersManager();
        }

        return _intance;
    }

    public void getFullProduct(String urlString, Action1<Product> onAddition) throws CannotObtainProductException {
        try {
            URL url = new URL(urlString);
            IProductObtainer obtainer = getObtainerDomain(url);

            new LoadProductAsyncTask(obtainer, onAddition).execute(url);
        }
        catch (Exception e) {
            throw new CouldNotParseUrlException(String.format("Cannot parse the url: %1$s", urlString), e);
        }
    }

    private IProductObtainer getObtainerDomain(URL url) throws CannotObtainProductException {
        String domain = url.getHost().split("\\.")[1];

        if(!obtainers.containsKey(domain)){
            throw new NotSupportedObtainerException(String.format("Cannot find the obtainer of domain: %1$s", domain));
        }

        return obtainers.get(domain);
    }
}
