package com.productstalker.activities.mainActivity;

import com.productstalker.models.Product;

import java.util.ArrayList;

import static java.lang.System.in;

/**
 * Created by Nirel on 26/10/2016.
 */

public class ProductsController {
    ArrayList<Product> productsList;

    public ProductsController(){
        productsList = new ArrayList<>();
    }

    public ArrayList<Product> getAllProducts(){
        return productsList;
    }

    public void addProduct(ArrayList<Product> newProducts){
        productsList.addAll(newProducts);
    }

    public void toggleFavorite(Product product) {
        product.isFavorite = !product.isFavorite;
    }

    public void remove(ArrayList<Integer> productsToRemove) {
        ArrayList<Product> removedObjects = new ArrayList<>();

        for(int index = 0; index < productsToRemove.size(); index++){
            removedObjects.add(productsList.get(index));
        }
        productsList.removeAll(removedObjects);
    }
}
