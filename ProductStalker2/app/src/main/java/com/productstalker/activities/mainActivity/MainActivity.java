package com.productstalker.activities.mainActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.productstalker.R;
import com.productstalker.fragments.addProductsFragment.AddProductFragment;
import com.productstalker.fragments.fullProductFragment.FullProductFragment;
import com.productstalker.fragments.productsListFragment.IProductObtainerActivity;
import com.productstalker.fragments.addProductsFragment.IProductUpdaterActivity;
import com.productstalker.fragments.productsListFragment.ProductsListFragment;
import com.productstalker.models.Product;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements IProductObtainerActivity, IProductUpdaterActivity {
    ProductsController productsController;
    public MainActivity(){
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        productsController = new ProductsController();

        handleStartIntent();
    }

    private void handleStartIntent() {
        startProductList();

        Intent openIntent = getIntent();
        if(openIntent != null && openIntent.getAction().equals(getString(R.string.shareIntent))){
            startProductAdd(openIntent.getStringExtra(Intent.EXTRA_TEXT));
        }
    }

    private void startProductList() {
        ProductsListFragment productsListFragment = new ProductsListFragment();

        Bundle fragmentsArguments = new Bundle();
        fragmentsArguments.putParcelableArrayList("products", productsController.getAllProducts());
        productsListFragment.setArguments(fragmentsArguments);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, productsListFragment, "productListPresenter")
                .commit();
    }

    private void startProductAdd(String sharedUrl) {
        AddProductFragment addProductFragment = new AddProductFragment();

        Bundle fragmentsArguments = new Bundle();
        fragmentsArguments.putString(getString(R.string.shared_url_arg_name), sharedUrl);
        addProductFragment.setArguments(fragmentsArguments);

        openAddProduct(addProductFragment);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onAddProductClicked(View view){
        openAddProduct(new AddProductFragment());
    }

    private void openAddProduct(AddProductFragment addProductFragment){
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.slide_in_with_fade, R.anim.fade_out, R.anim.fade_in, R.anim.slide_out_with_fade_out)
                .replace(R.id.fragment_container, addProductFragment)
                .addToBackStack("ProductListToAddProduct")
                .commit();
    }

    @Override
    public void addProducts(ArrayList<Product> newProducts) {
        productsController.addProduct(newProducts);

        getSupportFragmentManager().popBackStack("ProductListToAddProduct", FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public String getProductDetails(String url) {
        return url;
    }

    @Override
    public void toggleFavorite(Product product) {
        productsController.toggleFavorite(product);
    }

    @Override
    public void removeProducts(ArrayList<Integer> productsToRemove) {
        productsController.remove(productsToRemove);
    }

    @Override
    public void productClicked(Product product) {
        Fragment fullProductFragment = new FullProductFragment();
        FragmentManager fm = getSupportFragmentManager();

        FragmentTransaction transaction = fm.beginTransaction();

        setFullProductsArguments(fullProductFragment, product);
        transaction.setCustomAnimations(R.anim.slide_in_with_fade, R.anim.fade_out, R.anim.fade_in, R.anim.slide_out_with_fade_out);

        transaction.replace(R.id.fragment_container, fullProductFragment);
        transaction.addToBackStack("ProductListToAddProduct");

        transaction.commit();
    }

    private void setFullProductsArguments(Fragment fragment, Product product) {
        Bundle args = new Bundle();
        args.putParcelable("product", product);

        fragment.setArguments(args);
    }
}
