package com.productstalker.adapters.Exceptions;

/**
 * Created by Nirel on 31/10/2016.
 */

public class CannotLoadImageException extends IllegalArgumentException {
    public CannotLoadImageException(){
        super();
    }

    public CannotLoadImageException(String message){
        super(message);
    }

    public CannotLoadImageException(String message, Throwable cause){
        super(message, cause);
    }
}
