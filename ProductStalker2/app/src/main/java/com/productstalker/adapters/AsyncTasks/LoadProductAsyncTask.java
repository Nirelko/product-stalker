package com.productstalker.adapters.AsyncTasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;

import com.productstalker.adapters.Exceptions.CannotLoadImageException;
import com.productstalker.models.Product;
import com.productstalker.obtainers.apiObtainers.IProductObtainer;

import java.io.IOException;
import java.net.URL;

import rx.functions.Action1;

/**
 * Created by Nirel on 31/10/2016.
 */

public class LoadProductAsyncTask extends AsyncTask<URL, Void, Product> {
    private IProductObtainer _obtainer;
    private ArrayAdapter<Product> _productAdapter;
    private Action1<Product> _onFinish;

    public LoadProductAsyncTask(IProductObtainer obtainer, Action1<Product> onFinish){
        _obtainer = obtainer;
        _onFinish = onFinish;
    }

    @Override
    protected Product doInBackground(URL... productUrl) {
        try {
            Product product = _obtainer.getProduct(productUrl[0]);
            product.url = productUrl[0].toString();

            loadImages(product);

            return product;
        }
        catch (Exception e) {
            throw new CannotLoadImageException(String.format("Cannot get the image from the url: %1$s", productUrl[0].toString()), e);
        }
    }

    private void loadImages(Product product) throws IOException {
        product.thumbnailPic = loadImage(product.thumbnailPicUrl);

        product.ciricleThumbnailPic = generateCiricleBitmap(product.thumbnailPic);
    }

    private Bitmap loadImage(String thumbnailPicUrl) throws IOException {
        return BitmapFactory.decodeStream(new URL(thumbnailPicUrl).openConnection().getInputStream());
    }

    private Bitmap generateCiricleBitmap(Bitmap bmp) {
        int diameter = Math.max(bmp.getWidth(), bmp.getHeight());

        Bitmap resultBmp = Bitmap.createBitmap(diameter , diameter, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(resultBmp);
        Paint paint = getPaint(bmp);

        drawImageAsCircle(bmp, diameter, canvas, paint);

        return resultBmp;
    }

    private void drawImageAsCircle(Bitmap bmp, int diameter, Canvas canvas, Paint paint) {
        int horizonPadding = (diameter - bmp.getWidth()) / 2;
        int verticalPadding = (diameter - bmp.getHeight()) / 2;

        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawCircle(diameter / 2 + 0.7f, diameter / 2 + 0.7f,
                diameter / 2 + 0.1f, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bmp, horizonPadding, verticalPadding, paint);
    }

    @NonNull
    private Paint getPaint(Bitmap fittingBmp) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);
        paint.setColor(fittingBmp.getPixel(0, 0));

        return paint;
    }

    @Override
    protected void onPostExecute(Product product) {
        super.onPostExecute(product);

        _onFinish.call(product);
    }
}
