package com.productstalker.adapters;

import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Dean Kevorkian on 10/29/2016.
 */

public class CommitProductListItemHolder {
    ImageView thumbnailPic;
    TextView name;
    TextView price;
    ImageView inStock;
}
