package com.productstalker.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.productstalker.R;
import com.productstalker.models.Product;

import java.util.List;

/**
 * Created by Dean Kevorkian on 10/29/2016.
 * TODO: Rename this class to a more intuitive name.
 */

public class CommitProductListAdapter extends ArrayAdapter<Product> {
    protected Context context;
    protected int layout;
    protected List<Product> items;
    protected List<Integer> selectedItems;

    public CommitProductListAdapter(Context context, int resource, List items, List selectedItems) {
        super(context, resource, items);
        layout = resource;
        this.context = context;
        this.items = items;
        this.selectedItems = selectedItems;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // We check if the convertView is null. if it's not null, it means we
        // have the opportunity to use a used/already instantiated view to save memory (Android's handle).
        // If it's null, we initialize it and inflate it.

        // This holder class is used to keep the data of the line we represent.
        // In our case, each line keeps the name of the product (currently the URL) and the
        // thumbnail of the preview. (Also, to be added later, WIP).
        // After we instantiate it, we use "SetTag" to the View, to get hold of the data at
        // anytime we have the view itself!
        CommitProductListItemHolder holder = null;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(layout, parent, false);

            holder = bindHolderWithViews(convertView, position);

            // using setTag to keep the Data in the view that represents it, for later access.
            // No use for hashing & indexes since we keep the whole object at once.
            convertView.setTag(holder);
        }

        else{
            holder = (CommitProductListItemHolder)convertView.getTag();
        }

        setRowData(holder, items.get(position));

        setSelectedBackgroundColor(convertView, position);

        return convertView;
    }

    protected void setRowData(CommitProductListItemHolder holder, Product product) {
        holder.thumbnailPic.setImageBitmap(product.ciricleThumbnailPic);
        holder.name.setText(product.name);
        holder.price.setText(product.price.toString());

        if (product.isInStock) {
            holder.inStock.setImageResource(R.drawable.ic_in_stock);
            holder.inStock.setColorFilter(ContextCompat.getColor(context,R.color.colorInStock));
        }
        else {
            holder.inStock.setImageResource(R.drawable.ic_out_of_stock);
            holder.inStock.setColorFilter(ContextCompat.getColor(context,R.color.colorOutStock));
        }
    }

    @NonNull
    protected CommitProductListItemHolder bindHolderWithViews(View convertView, int position) {
        CommitProductListItemHolder holder = createNewHolder();

        holder.thumbnailPic = (ImageView) convertView.findViewById(R.id.productListImage) ;
        holder.name = (TextView) convertView.findViewById(R.id.productListName);
        holder.price = (TextView) convertView.findViewById(R.id.productListPrice);
        holder.inStock = (ImageView) convertView.findViewById(R.id.productListInStockImage);

        return holder;
    }

    protected CommitProductListItemHolder createNewHolder(){
        return new CommitProductListItemHolder();
    }

    private void setSelectedBackgroundColor(View selectedItemView, int position) {
        if(selectedItems.contains(position)) {
            selectedItemView.setBackgroundColor(ResourcesCompat.getColor(context.getResources(), R.color.colorBackgroundSelected, null));
        }
        else{
            selectedItemView.setBackgroundColor(ResourcesCompat.getColor(context.getResources(), R.color.colorBackground, null));
        }
    }

    public Product getProduct(int position){
        return this.items.get(position);
    }
}
