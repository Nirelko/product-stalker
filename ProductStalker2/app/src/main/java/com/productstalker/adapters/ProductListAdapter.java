package com.productstalker.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.productstalker.R;
import com.productstalker.fragments.productsListFragment.IProductObtainerActivity;
import com.productstalker.models.Product;
import com.productstalker.obtainers.apiObtainers.IProductObtainer;

import java.util.List;

/**
 * Created by Nirel on 31/10/2016.
 */

public class ProductListAdapter extends  CommitProductListAdapter {

    public ProductListAdapter(Context context, int resource, List objects, List selectedItems) {
        super(context, resource, objects, selectedItems);
    }

    @Override
    protected void setRowData(CommitProductListItemHolder holder, Product product) {
        super.setRowData(holder, product);

        ProductListItemHolder fullHolder = (ProductListItemHolder)holder;

        applyFavoriteColor(product.isFavorite, fullHolder);
    }

    @NonNull
    @Override
    protected CommitProductListItemHolder bindHolderWithViews(View convertView, int position) {
        ProductListItemHolder holder = (ProductListItemHolder)super.bindHolderWithViews(convertView, position);
        bindFavoriteButton(convertView, holder, position);

        return holder;
    }

    private void bindFavoriteButton(View convertView, final ProductListItemHolder holder, final int position) {
        holder.favorite = (ImageButton) convertView.findViewById(R.id.productListFavorite);

        holder.favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Product product = items.get(position);
                ((IProductObtainerActivity)context).toggleFavorite(product);

                applyFavoriteColor(product.isFavorite, holder);
            }
        });
    }

    private void applyFavoriteColor(boolean isFavorite, ProductListItemHolder holder) {
        if(isFavorite){
            holder.favorite.setColorFilter(ContextCompat.getColor(context, R.color.colorAccentLight));
        }
        else{
            holder.favorite.setColorFilter(0);
        }
    }

    @Override
    protected CommitProductListItemHolder createNewHolder() {
        return new ProductListItemHolder();
    }
}
